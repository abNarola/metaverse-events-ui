import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);


const routes = [
  {
    path: "/",
    name: "Dashboard",
    component: () => import("@/views/Dashboard.vue"),
    meta: {
      component:'Dashboard',
    }
  },
  {
    path: "/add-event",
    name: "Events",
    component: () => import("@/views/Events.vue"),
    meta: {
      component:'Events',
    }
  },
];

const router = new VueRouter({
  routes: routes,
  mode: "history",
});

router.beforeEach((to, from, next) => {
  // console.log('to => ', to);
  // console.log('to => ', from);
  return next();
});

export default router;
