import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify, {
  theme: {
    light: {
      primary: "e17954"
    },
  },
});

export default new Vuetify({});
